\documentclass[a4paper,11pt]{article}

% required packages
\usepackage[utf8x]{inputenc}
\usepackage{times}
\usepackage{paralist}
\usepackage{color}

\newcommand\blfootnote[1]{%
  \begingroup
  \renewcommand\thefootnote{}\footnote{#1}%
  \addtocounter{footnote}{-1}%
  \endgroup
}

%opening
\title{\textcolor{blue}{110231/110233 Applied Dynamical Systems + Lab}}
\author{Marc-Thorsten H\"utt and Marcel Oliver}
\date{\today}

\begin{document}

\maketitle


\section{Course description}

Applied Dynamical Systems together with the associated Lab serves as a
first problem-oriented introduction to nonlinear dynamics. It also
provides the foundation for the theoretical lecture course
\emph{Dynamical Systems and Control} in Fall. Nonlinear phenomena will
be explored both in the laboratory and on the computer. The lab will
cover real-world examples of nonlinear dynamics experiments such as
nonlinear electric oscillators and pattern formation in chemical
reactions, as well as some paradigmatic models of nonlinear dynamics.

Programming environments will be Scientific Python for number
crunching and Mathematica for symbolic computing.

A main focus of the lab is the development of standard tools for the
numerical solution of differential equations, the application of
automated tools for bifurcation analysis, and continuation methods.
We will also implement simple agent-based models and pseudo-spectral
PDE solvers for reaction-diffusion equations.

%Students wishing to hear about the theoretical foundations of some of
%the topics simultaneously are suggested to also take \emph{Applied
%Differential Equations and Modeling}, \emph{Ordinary Differential
%Equations and Dynamical Systems} and/or \emph{Applied Stochastic
%Processes}.

Participants should be prepared to present their report and/or program
code to the instructor or the entire class; this presentation may be
part of the grading scheme of a particular report.


\section{Structure of the lab protocols}

After each ``segment'' (1--3 topically linked lab sessions)
participants are required to submit a lab report within \textbf{five
days}.

This may include a lab report as well as an independent file
containing the accompanying program code with which the ``Results''
figures from your report have been generated. These program files
should run without modification and produce the results claimed in the
report. Assignments for which both a report and the code are due are
indicated by ``Report'' on the schedule; those for which the code file
only is required are indicated by ``Code''.

Lab reports should be typeset in {\LaTeX } and submitted
electronically as a single pdf document.  Your code should be
appropriately commented to explain the purpose of each (non-trivial)
step of your workflow. Inadequately commented code will result in
deduction of a grade equivalent of 0.33; if, in addition, the code
structure is not clear and transparent (e.g. misleading variable
names), a further 0.33 may be deducted.
 
%Both, lab reports and program codes are to be submitted to Marc
%H\"utt (\textcolor{blue}{m.huett@jacobs-university.de}) and Marcel
%Oliver (\textcolor{blue}{m.oliver@jacobs-university.de}) 

Reports should be as brief as possible (3--4 pages can sometimes be
enough) without omitting essential information or discussions.  You
don't need to write a text book!

For every day after the deadline, a grade equivalent of 0.33 will be
deducted from the grade achieved for the assignment.

For grading, code assignments will be weighted as 0.5 of assignments
consisting of a lab report and code. Where both a report and code are
submitted, the report has the higher importance.
 
One report may be omitted from grading.  This covers all minor
emergencies including minor illness and extracurricular activities.
Further exceptions will only be made in truly extenuating and
well-documented circumstances.

The structure of the lab protocol resembles that of a short scientific paper: 
\begin{enumerate}
\item Title
\item Author
\item Abstract (briefly summarizing the goal and the result in 3-5 sentences)
\item Introduction (including a brief summary of the mathematical and
scientific background, as well as the history of the problem, if
applicable)
\item Methods (including details about the implementation, programming
strategy; in the ``Methods'' section you should describe exactly what
you have been doing: which algorithm, what parameters, experimental
setup, what postprocessing/visualization of the data, etc.)
\item Results 
\item Conclusion (summarizing your findings and embedding them in a broader perspective)
\item References
\end{enumerate}

All references, online or paper, must be included in the list of
references. All help received must be acknowledged, including code
segments taken from peers. There is no grade penalty for properly
credited help, as long as the submission as a whole remains in essence
an independent original contribution. Help or ``borrowed code''
without attribution is plagiarism and will be pursued according to the
Code of Academic Integrity.

\newpage

\section{Structure of the course (subject to change)}

\begin{itemize}
\item Tuesday sessions: 8:15--11:00 in East Hall 4
\item Thursday sessions: 11:15--12:30 in East Hall 8
\end{itemize}

\begin{tabular}{lllll}
\textbf{\textcolor{blue}{Date}}         &
\textbf{\textcolor{blue}{Instr.}}       &       
\textbf{\textcolor{blue}{Task}}       &       
\textbf{\textcolor{blue}{Topic}}        \\
Th, 2. Feb.   &  MO  &        & Introduction I \\
Tue, 7. Feb.  &  MTH &        & Introduction II \\
Th, 9. Feb.   &  MTH & Code   & Programming intro (Mathematica)  \\
Tue, 14. Feb. &  MTH &        & Programming intro (Mathematica)  \\
Th, 16. Feb.  &  MO  & Code   & Programming intro (Python) \\
Tue, 21. Feb. &  MO  &        & Programming intro (Python) \\
Th, 23. Feb.  &  MO  &        & ODE solvers: Theory \\
Tue, 28. Feb. &  MO  & Code$^*$ & ODE solvers: Applications \\
Th, 2. Mar.   &  MO  &        & Chua's circuit: Theory \\
Tue, 7. Mar.  &  MO  &        & Chua's circuit: Experiment \\
Th, 9. Mar.   &  MO  & Report & Chua's circuit: Computational discussion \\
Tue, 14. Mar. &  --  &        & Homework session \\
Th, 16. Mar.  &  MTH &        & Stability analysis of a simple neuron model \\
Tue, 21. Mar. &  MTH & Code   & Basic principles of spiral wave formation \\
Th, 23. Mar.  &  MTH &        & Biochemical switches and oscillators I \\
Tue, 28. Mar. &  MTH & Report & Biochemical switches and oscillators II \\
Th, 30. Mar.  &  TP  &        & Finite difference discretization of 1D steady state diffusion \\
Tue, 4. Apr.  &  TP  &        & Diffusion equation \\
Th, 6. Apr.   &  TP  & Report & Simple reaction-diffusion model \\
Tue, 18. Apr. &  AM  &        & Daisyworld -- Biological homeostasis in an idealized world I \\
Th, 20. Apr.  &  AM  & Report & Daisyworld II \\
Tue, 25. Apr. &  MTH &        & Excitable dynamics in networks \\
Th, 27. Apr.  &  MO  &        & Belousov-Zhabotinsky reaction: Experiment \\
Tue, 2. May   &  MO  &        & Belousov-Zhabotinsky reaction: Theory \\
Th, 4. May    &  MO  & Report & Belousov-Zhabotinsky reaction: Computational discussion \\
Tue, 9. May   &  --  &        & Homework session \\
Th, 11. May   &  MTH &        & Cellular automata: Wolfram classes \\
Tue, 16. May  &  MTH & Code   & Cellular automata: Edge of chaos \\
\end{tabular}
\bigskip

\noindent\emph{Note:} When a task is assigned, you have \textbf{five} more days to work on it, it is due by midnight of the last of these five days.

\bigskip (*) Code assignment added after publication of syllabus.
Thus, one of the code assignments will be dropped from grading.

\end{document}
