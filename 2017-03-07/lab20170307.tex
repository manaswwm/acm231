\documentclass[12pt]{article}
\usepackage{fullpage,amsmath,amsfonts}

\def\d{{\mathrm d}}
\def\c{{\mathrm c}}
\def\nl{{\operatorname{nl}}}

\begin{document}

\title{Nonlinear Dynamics Lab}
\author{Session 9--10}
\date{due March 19, 2014}
\maketitle



\section{Chua's Circuit}


This weeks lab sessions study \emph{Chua's Circuit}, a nonlinear
electric oscillator which can be assembled with standard electronics
parts.  For background and circuit diagrams, consult the article by
Hobson and Lansbury \cite{HL96}.

One of the crucial ingredients for the circuit is the inductor $L$.
It is often necessary or convenient to use commodity inductors, which
are typically far from ideal.  In particular, real-world inductors
have a resistance $R_L$ which is typically non-negligible for the
operation of the Chua oscillator.  Other non-ideal effects include
parasitic capacitance, radiative losses and hysteresis which we shall
ignore to keep the modeling simple; however, these effects may still
be significant.

To include the effect of the inductor's resistance into the
mathematical description, we analyze it as a resistor in series with
an ideal inductor.  Consequently, the differential equations
describing Chua's Circuit read
\begin{align}
  C_1 \, \frac{\d V_1}{\d t}
  & = \frac{V_2 - V_1}{R_\c} - I_\nl (V_1) \,, \\
  C_2 \, \frac{\d V_2}{\d t}
  & = \frac{V_1 - V_2}{R_\c} + I_L \,, \\
  L \, \frac{\d I_L}{\d t}
  & = - V_2 - R_L \, I_L \,.
\end{align}
The resistance $R_\c$ is the coupling resistor, labeled $1/G$ in
Hobson and Lansbury's article.  A short derivation of these equations
should be contained in your lab report.

\section{Preparatory tasks}

\emph{Note}: Everybody should have made significant progress with the
following tasks before the experimental session.  They will be part of
the lab report, to be submitted in groups of two.  Code-sharing within
the group is permitted.  However, it is expected that every
participant is familiar enough with the task that he/she could have
written the code on their own and is able to run, modify, and explain
the code on request.

\begin{enumerate}
\item Define a function for the nonlinear resistor whose transfer
characteristic $I_\nl(V)$ is depicted in Figure~2 of Hobson and
Lansbury \cite{HL96}.  (E.g., define parameters for the slopes and the
location of the kink.)

\item Code up a solver for the Chua system, either by modifying your
solver from last week, or by using one of the build-in solvers from
\texttt{scipy.integrate.ode}.  (The latter is likely more robust and
faster.)  For values of the various parameters, see below.

\item Something to think about: what should you plot as the output of
your simulation in order to replicate the picture seen on the $X$-$Y$
oscilloscope attached to the circuit as depicted in \cite{HL96}?

\end{enumerate}


\section{Lab tasks}

\begin{enumerate}
\item The inductor we have readily available is one with $10 \,
\mathrm{mH}$. Take a multimeter and measure its resistance $R_L$.

\item Assemble Chua's circuit.  Since the inductor is different from
the one used in \cite{HL96}, some resistances should be chosen
differently from the circuit diagram: take
$R_1 = 2.2 \, \mathrm{k\Omega}$ and $R_4 = 1 \,\mathrm{k\Omega}$, the
other values as in the diagram.  For the variable resistance $R_\c$ we
use a multiturn potentiometer which allows fine control of its
resistance.

\item Determine experimentally the value of $R_\c$ which corresponds
to the onset of chaos.  Do you see a sharp transition, or a period
doubling cascade as for the logistic map?

\item Measure the response curve of each of the nonlinear inverse
resistors.  To do so, use a third operational amplifier as a voltage
follower attached to the noninverting inputs of the first two
operational amplifiers.  To the output of this voltage follower, you
can attach the oscilloscope ground.  The X- and Y-channels of the
oscilloscope can then be attached to ground to measure $-V_1$ and to
the far end of $R_3$ (or $R_6$).  The voltage across $R_3$ resp.\
$R_6$ can be converted into the current response of the corresponding
nonlinear resistor via Ohm's law.

\item If you have time: Use an inductor with $100 \, \mathrm{mH}$,
also available in the lab, and try if you can---potentially modifying
the values of the capacitors or resistors as well---find a regime as
well.  If this is successful, you will likely obtain less hysteresis
in the response curve of the nonlinear resistors, and consequently
better agreement of theory and experiment.  (Note: the $100 \,
\mathrm{mH}$ inductor might have too big a resistance to excite
nonlinear oscillations, so this is not guaranteed to work!)

\end{enumerate}

\section{Report items}

\begin{enumerate}

\item Analyze the ``inverse resistor'' consisting of $R_1$, $R_2=R_3$,
and the operational amplifier.  Write out an expression for the
current response as a function of the input voltage, assuming that the
operational amplifier saturates at output voltage $\pm V_{\max}$.
Write out and plot an expression for the overall response curve of the
two parallel inverse resistors used in Chua's circuit.

\item Use the two experimentally response curves to obtain an
approximate measured response curve.  Plot the theoretical curve and
the measured curve in one coordinate system.

\item Write a program which simulates Chua's circuit over a time
interval $T=0.02 \, \mathrm{s}$.  Plot $V_1$ vs.\ $V_2$ for times
$t=[T/2,T]$, thereby discarding transients.  Find the value for $R_\c$
at the onset of chaos.  (Both for the theoretical response curve and
for the reconstruction of the measured curve where you may ignore
hysteresis effects.)

\item Compare the experimental with the numerical results and discuss
possible differences.


\end{enumerate}

The experiment and the lab report may be done in groups of two.



\section{Parts list}

\begin{itemize}
\item 1 Capacitor $10\,\mathrm{nF}$
\item 3 Capacitors $100\,\mathrm{nF}$
\item 1 Inductor $10 \, \mathrm{mH}$ 
\item 2 Resistors $220 \, \mathrm{\Omega}$ (rd-rd-bk-bk-br)
\item 1 Resistor $1 \, \mathrm{k\Omega}$ (br-bk-bk-br-br)
\item 1 Resistor $2.2 \, \mathrm{k\Omega}$ (rd-rd-bk-br-br)
\item 2 Resistors $22 \, \mathrm{k\Omega}$ (rd-rd-bk-rd-br)
\item 3 Operational Amplifiers LM741
\item 1 Multi-turn potentiometer $1 \, \mathrm{k\Omega}$
\end{itemize}

\begin{thebibliography}{99}
\bibitem{HL96}
  P.R. Hobson and A.N. Lansbury,
  \textit{A simple electronic circuit to demonstrate bifurcation and
  chaos}, 
  Phys. Educ. \textbf{31} (1996), 39--43. \\
  


\end{thebibliography}




\end{document}

