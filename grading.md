# Grading


## Task 01
* no submission

* 0/5


## Task 05
* no git submission
* exercise 1
    * no influence of b/c investigated
    * no trajectories simulated
* exercise 2
    * no phase plane plot/detailed explanation
* exercise 3
    * Range[-0.2, 0.2] only generates one number (in a list), try e.g. Range[-0.2, 0.2, 0.05] instead
    * no interpretation of results
* try to improve the structure of your program/level of detail in your explanations

* 2/5


## Task 06
* no submission

* 0/5


## Task10
* no submission

* 0/5
